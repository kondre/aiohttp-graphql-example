import random
from aiohttp import web
from aiohttp_graphql import GraphQLView
from graphql import GraphQLArgument, GraphQLField, GraphQLInt, GraphQLList, GraphQLObjectType, GraphQLSchema, GraphQLString

app = web.Application()

books = [
    {
        "name":"«Становление»", 
        "author": "Мишель Обама",
    },
    {
        "name":"«Четыре соглашения»", 
        "author": "Дон Мигель Руис",
    },
    {
        "name":"«Весь невидимый нам свет»", 
        "author": "Энтони Дорр",
    },
    {
        "name":"«Книжный вор»", 
        "author": "Маркус Зусак",
    },
]    

def random_book_resolver(root, info):
    return [random.choice(books)]

def all_books_resolver(root, info, count):
    return books[:count]

BooksListType = GraphQLObjectType(
    name="BooksListType",
    fields={
        "name" : GraphQLField(GraphQLString),
        "author" : GraphQLField(GraphQLString)
    }
)

schema = GraphQLSchema(
    query=GraphQLObjectType(
        name="RootSchemaType",
        fields={
            "randomBook": GraphQLField(
                GraphQLList(BooksListType), 
                resolver=random_book_resolver
            ),
            "allBooks": GraphQLField(
                GraphQLList(BooksListType), 
                resolver=all_books_resolver, 
                args={"count" : GraphQLArgument(GraphQLInt, default_value=3)}
            )
        }
    )
)

GraphQLView.attach(app, schema=schema, graphiql=True)

# Optional, for adding batch query support (used in Apollo-Client)
# GraphQLView.attach(app, schema=schema, batch=True, route_path="/graphql/batch")

if __name__ == '__main__':
    web.run_app(app)